'use strict';

var _ = require('underscore');
//const emitty = require('emitty').setup(_.gulp.localSourcePath, 'pug');

_.gulp.task('local:markup:dist', function () {

	//console.log(global.watch);

	//console.log(global.emittyChangedFile);

	//return;

	/**
	 * Collect pug files and compile them in html
	 */
	return _.gulp.src(_.gulp.localSourcePath + '/**/*.pug')
		//.pipe(_.cache('local:markup:dist'))
		//.pipe(_.cached('local:markup:dist'))
		//.pipe(_.gulpif(global.watch, _.emitty.stream()))
		//.pipe(_.gulpif(global.watch, _.emitty.stream(global.emittyChangedFile)))

		.pipe(_.cached('local:markup:dist'))
		.pipe(_.progeny())

		//.pipe(_.pugInheritance({basedir: 'local/src/'}))

		.pipe(_.pug({
			pretty: true,
			//compileDebug: true
		}))
		//.pipe(_.remember('local:markup:dist'))
		.pipe(_.gulp.dest(_.gulp.localDistributivePath + '/markups'));

});