'use strict';

var _ = require('underscore');

_.gulp.task('local:sprite:dist', function () {

	var timestamp = Date.now();

    function spriteScale(callback) {
        _.gulp.src(_.gulp.localSourcePath + '/../sprites/*.png')
            .pipe(_.imageResize({
                percentage: 200,
                imageMagick: true
            }))
            .pipe(_.rename(function (path) { path.basename += "@2x"; }))
            .pipe(_.gulp.dest(_.gulp.localSourcePath + '/../sprites@2x'))
            .on('end', callback)
    }

    function spriteConver() {
        var spriteData = _.gulp.src([
            _.gulp.localSourcePath + '/../sprites/*.png',
            _.gulp.localSourcePath + '/../sprites@2x/*.png'
        ])
            .pipe(_.spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.styl',
                imgPath: '../assets/sprites/sprite.png?' + timestamp,
                padding: 2,
                algorithm: 'top-down',
                retinaSrcFilter: [_.gulp.localSourcePath + '/../sprites@2x/*.png'],
                retinaImgPath: '../assets/sprites/sprite@2x.png?' + timestamp,
                retinaImgName: 'sprite@2x.png'
            }));

        spriteData.img.pipe(_.gulp.dest( _.gulp.localDistributivePath + '/sprites'));
        spriteData.css.pipe(_.gulp.dest(_.gulp.localSourcePath + '/styles/sprite'));

        return spriteData;
    }

    return spriteScale(spriteConver);

});

_.gulp.task('local:sprite:dest', function () {

	/**
	 * Copy sprite to distr
	 *
	 * @use:
	 * _.gulp.localDistributivePath
	 * _.gulp.markupPath
	 *
	 */

	return _.gulp.src(_.gulp.localDistributivePath + '/sprites/*.png' )
		.pipe(_.gulp.dest(_.gulp.markupPath + '/assets/sprites'));
});