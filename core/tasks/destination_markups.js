'use strict';

var _ = require('underscore');

_.gulp.task('dist:markup:dest', function () {

	/**
	 * Copy html page files to output destination
	 */
	return _.gulp.src(_.gulp.localDistributivePath + '/markups/' + _.config.path.markupInclude + '/**/*.html')
		.pipe(_.gulp.dest(_.gulp.markupPath + '/' + _.config.path.markupInclude));

});