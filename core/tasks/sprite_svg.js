'use strict';

var _ = require('underscore');

_.gulp.task('local:sprite:svg:dist', function () {
	return _.gulp.src(_.gulp.localSourcePath + '/../sprites-svg/*.svg')
        .pipe(_.svgmin(function (file) {
          return {
            plugins: [{
              cleanupIDs: {
                minify: true
              }
            }]
          }
        }))
        .pipe(_.svgstore({ inlineSvg: true }))
        .pipe(_.gulp.dest(_.gulp.localSourcePath + '/../sprites-svg-min/'));

});


_.gulp.task('local:sprite:svg:dest', function () {

	return _.gulp.src(_.gulp.localSourcePath + '/../sprites-svg-min/*.svg' )
		.pipe(_.gulp.dest(_.gulp.markupPath + '/assets/sprites'));

});