'use strict';

var _ = require('underscore');

_.gulp.task('local:styles:collect:main', function () {

	/**
	 * Collect stylus styles to style.css
	 */
	return _.gulp.src([
		//'core/stylus/app.styl',
		_.gulp.localSourcePath + '/styles/main.styl',
		'!' + _.gulp.localSourcePath + '/styles/**/!(main.styl)'
		//_.gulp.localSourcePath + '/**/*.styl'
	])
	//.pipe($.concat('main.styl'))
		.pipe(_.concat.header('@import "jeet"\n'))
		.pipe(_.concat.header('@import "kouto-swiss"\n'))
		.pipe(_.stylus({
			//import: ['../../../node_modules/jeet/jeet'],
			use: [_.koutoSwiss(), _.jeet()]
		}))
		.pipe(_.$.concat('main.css'))
		.pipe(_.gulp.dest(_.gulp.localDistributivePath + '/styles'));

});

_.gulp.task('local:styles:collect:vendor', function () {

	/**
	 * Collect local vendor css files to vendor.css
	 */
	if(_.config.vendor.styles) {

		var isHasAdjustParams;
		var arCssPluginsSrc = [];

		var vendorStyles = _.config.vendor.styles;
		for (var cssPlugin in vendorStyles) {
			isHasAdjustParams = false;
			if(vendorStyles[cssPlugin].path) {
				//arCssPluginsSrc.push('./local/' + vendorStyles[cssPlugin].path);

				if(vendorStyles[cssPlugin].adjustParam) {
					isHasAdjustParams = true;
				}

				_.gulp.src('./local/' + vendorStyles[cssPlugin].path)
				//.pipe($.concat('vendor.css'))
					.pipe(_.gulpif(isHasAdjustParams, _.$.cssUrlAdjuster(vendorStyles[cssPlugin].adjustParam)))
					.pipe(_.gulp.dest(_.gulp.localDistributivePath + '/styles/vendor/'));
			}
		}

		/*
		 if(arCssPluginsSrc.length > 0){
		 gulp.src(arCssPluginsSrc)
		 //.pipe($.concat('vendor.css'))
		 .pipe(gulp.dest(gulp.localDistributivePath + '/styles/vendor/'));
		 }
		 */
	}

	return true;

});