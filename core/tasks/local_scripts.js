'use strict';

var _ = require('underscore');

_.gulp.task('local:scripts:collect:vendor', function () {

	/**
	 * Collect local vendor js files to vendor.js
	 */
	if(_.config.vendor.scripts) {

		var arJsPluginsSrc = [];

		var vendorScripts = _.config.vendor.scripts;
		for (var jsPlugin in vendorScripts) {
			if(vendorScripts[jsPlugin].path) {
				arJsPluginsSrc.push('./local/' + vendorScripts[jsPlugin].path);
			}
		}

		if(arJsPluginsSrc.length > 0){
			_.gulp.src(arJsPluginsSrc)
				.pipe(_.$.concat('vendor.js'))
				.pipe(_.gulp.dest(_.gulp.localDistributivePath + '/scripts'));
		}

	}

	return true;
	
});

_.gulp.task('local:scripts:dist', function () {

	/**
	 * Collect local project js files to distr
	 */
	return _.gulp.src([
		_.gulp.localSourcePath + '/**/*.js'
	])
		.pipe(_.gulp.dest(_.gulp.localDistributivePath + '/scripts'));

});