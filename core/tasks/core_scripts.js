'use strict';

var _ = require('underscore');

_.gulp.task('core:scripts:collect:core', function () {

	/**
	 * Collect core js files to core.js
	 */
	return _.gulp.src([
		'node_modules/underscore/underscore.js',
		'node_modules/js-cookie/src/js.cookie.js',
		'node_modules/jquery/dist/jquery.js',
		'node_modules/jquery-migrate/dist/jquery-migrate.min.js',
		'node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js',
		'node_modules/inputmask/dist/min/inputmask/phone-codes/phone.min.js',
		'node_modules/inputmask/dist/min/inputmask/phone-codes/phone-ru.min.js',
		'node_modules/parsleyjs/dist/parsley.min.js',
		'node_modules/jquery-form/dist/jquery.form.min.js',
		'node_modules/backbone/backbone.js',
		'core/javascript/app.js',
		'core/javascript/extension/**/*.js',
		'core/javascript/superstructure/**/*.js'
	])
		.pipe(_.$.concat('core.js'))
		.pipe(_.gulp.dest('./core/javascript/'));

});

_.gulp.task('core:scripts:copy:dist', function () {

	/**
	 * Copy core.js to distr
	 */
	return _.gulp.src('./core/javascript/core.js')
		.pipe(_.gulp.dest(_.gulp.localDistributivePath + '/scripts'));

});
