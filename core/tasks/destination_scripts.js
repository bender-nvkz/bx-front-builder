'use strict';

var _ = require('underscore');

_.gulp.task('dist:scripts:dest', function () {

	/**
	 * Collect and copy js files to output destination
	 */
	_.gulp.src([
		_.gulp.localDistributivePath + '/scripts/core.js',
		_.gulp.localDistributivePath + '/scripts/vendor.js'
	])
		.pipe(_.gulp.dest(_.gulp.markupPath + '/js'));

	return _.gulp.src([
		_.gulp.localDistributivePath + '/scripts/scripts/main.js',
		_.gulp.localDistributivePath + '/scripts/scripts/*.js',
		_.gulp.localDistributivePath + '/scripts/global/**/*.js',
		_.gulp.localDistributivePath + '/scripts/**/*.js',
		'!' + _.gulp.localDistributivePath + '/scripts/!(core.js)',
		'!' + _.gulp.localDistributivePath + '/scripts/!(vendor.js)'
	])
		.pipe(_.$.concat('main.js'))
		.pipe(_.gulp.dest(_.gulp.markupPath + '/js'));
	
});