'use strict';

var _ = require('underscore');

_.gulp.task('dist:styles:dest', function () {

	/**
	 * Copy style files to output destination
	 */

	return _.gulp.src([
		_.gulp.localDistributivePath + '/styles/vendor/*.css',
		_.gulp.localDistributivePath + '/styles/main.css'
	])
		.pipe(_.$.concat('style.css'))
		.pipe(_.gulp.dest(_.gulp.markupPath + '/css'));
	
});