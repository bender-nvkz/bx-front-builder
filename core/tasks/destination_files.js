'use strict';

var _ = require('underscore');

_.gulp.task('dist:vendor:dest', function () {

	/**
	 * Copy vendor files to output destination
	 */
	return _.gulp.src([
		_.gulp.localDistributivePath + '/vendor/**/*'
	])
		.pipe(_.gulp.dest(_.gulp.markupPath + '/vendor'));

});