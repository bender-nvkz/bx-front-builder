'use strict';

var _ = require('underscore');

_.gulp.task('local:assets:dest', function () {

	/**
	 * Copy assets files to output destination
	 */
	return _.gulp.src([
		_.gulp.localAssetsPath + '/**/*'
	])
		.pipe(_.gulp.dest(_.gulp.markupPath + '/assets'));
	
});

_.gulp.task('local:vendor:dist', function () {

	/**
	 * Copy vendor files to distr
	 */
	if(_.config.vendor.files) {
		var vendorFiles = _.config.vendor.files;
		for (var filesPlugin in vendorFiles) {
			_.gulp.src('./local/' + vendorFiles[filesPlugin].src)
				.pipe(_.gulp.dest(_.gulp.localDistributivePath + '/vendor/' + vendorFiles[filesPlugin].dest));
		}
	};
	
	return true;

});
