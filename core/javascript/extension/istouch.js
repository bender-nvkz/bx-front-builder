if (!_(Fx).has('isTouch')) {
	Fx.isTouch = function () {
		return !!(('ontouchstart' in window)
		|| navigator.maxTouchPoints
		|| (window.DocumentTouch && document instanceof DocumentTouch));
	};
}
