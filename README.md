# Interface building for Bitrix app.
**Short guide. More instruction comes later.**

## What is in the box?

Gulp tasks  
Backbone js apps (Underscore, jQuery3, JSCookie)  
Pug templating (Jade)  
Stylus styles support (with Kouto Swiss tollbox and build in Jeet grid system)  
Sprite image and stylus css-processor rules/mixins generation  
with function of automatic generation retina images (by the scaling original)  

### New in v0.1.0

Integrated dependencies (allready in the core):  
[Jquery.inputmask](http://robinherbots.github.io/Inputmask/) - a jquery plugin which create an input mask (phone codes handler include in core);  
[Parsley](http://parsleyjs.org/) - ultimate JavaScript form validation library;  
[jQuery Form Plugin](https://jquery-form.github.io/form/) - allows you to easily and unobtrusively upgrade HTML forms to use AJAX;  


## Development plans:

Create deployment tasks for integrate builds right inside templates.  
Add bem shortcuts to pug.  
Localserver and Browsersync modules.  
Linters, minifiers, prefixers, some postprocessing and cross-browser helpers.  

## How to...?

1. Create "local" directory inside root-cat.  
2. Place your source files on this structure template:  
```
├── assets/                    # Project files (images, fonts, backgrounds)
├── sprites/                   # Sprite images (not supporting now)
├── src/                       # Source files
│   ├── blocks/                # Blocks (pug, html, js, styl)
│   ├── pages/                 # Pages
│   ├── scripts/               # JavaScript files
│   │   └── main.js            # Main script
│   ├── styles/                # Stylus style files
│   │   └── main.styl          # Main stylus file
│   └── ../**.*                # Any other files inside any dir (pug, html, js, styl)
├── bower.json                 # *Optional
├── config.json                # Builder config
```
3. For using sprite generation make sure [ImageMagick](http://www.imagemagick.org/) is [installed](http://www.imagemagick.org/script/binary-releases.php) on your system and properly set up. 

### Available commands list
```
build
markup
styles
scripts
styles:main
scripts:main
scripts:vendor
files
core
```

### You can fork this repo and development your own interface fully, but i'm strongly recommend to clone repo, and init new git repo inside local folder.
Also you can check dull template, which can be tested for first start here:  
[bx-front-dull](https://bitbucket.org/bender-nvkz/bx-front-dull)