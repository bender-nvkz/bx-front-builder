'use strict';

/**
 * @todo: adjust URL to relative css and js files
 * @todo: remove comments from dest codes
 * @todo: Add bem shortcuts to pug - https://www.npmjs.com/package/pug-bem-lexer
 * @todo: Localserver autostart
 * @todo: Browsersync: https://github.com/BrowserSync/gulp-browser-sync
 *
 * Reserve:
 *
 * Pug -->
 * https://www.npmjs.com/package/stylinpug - Check Stylus against Pug, and vice versa, for unused and blacklisted
 *     classes https://www.npmjs.com/package/gulp-html2pug - Gulp plugin to convert html files to pug
 *     https://www.npmjs.com/package/pug-php-filter - A php filter for pug https://www.npmjs.com/package/pug-load - The
 *     Pug loader is responsible for loading the depenendencies of a given Pug file
 *     https://www.npmjs.com/package/pug-parser - may be useful for dump: The pug parser (takes an array of tokens and
 *     converts it to an abstract syntax tree) https://www.npmjs.com/package/pug-error - Standard error objects for pug
 *
 *
 * Stylus -->
 * https://www.npmjs.com/package/gulp-stylint - Gulp plugin for stylus stylint linter
 * https://www.npmjs.com/package/gulp-csscomb - form distr version: CSScomb is a coding style formatter for CSS
 * https://www.npmjs.com/package/autoprefixer - Parse CSS and add vendor prefixes to CSS rules using values from the
 *     Can I Use website
 *
 *
 * HTML -->
 * https://www.npmjs.com/package/gulp-htmlhint - htmlhint wrapper for gulp to validate your HTML
 *
 *
 * CSS -->
 * https://www.npmjs.com/package/gulp-csslint - CSSLint plugin for gulp
 * https://www.npmjs.com/package/gulp-csso - Minify CSS with CSSO
 * https://www.npmjs.com/package/gulp-group-css-media-queries - CSS postprocessing: group media queries. Useful for
 *     postprocessing preprocessed CSS files
 *
 *
 * JS -->
 * https://www.npmjs.com/package/gulp-autopolyfiller - Autopolyfiller plugin for Gulp
 * https://www.npmjs.com/package/gulp-babel - Use next generation JavaScript, today
 * https://www.npmjs.com/package/gulp-jsbeautifier - jsbeautifier.org for Gulp
 * https://www.npmjs.com/package/gulp-jscs - Check JavaScript code style with jscs
 * https://www.npmjs.com/package/gulp-uglify - Minify files with UglifyJS
 * https://www.npmjs.com/package/gulp-eslint - A gulp plugin for processing files with ESLint
 *
 *
 */

//const $$ = require('auto-require')();
var _ = require('underscore');
_.gulp = require('gulp');
_.trycatch = require('trycatch');
_.requireDir = require('require-dir');
_.runSequence = require('run-sequence');
_.appVer = '0.1.1';
_.concat = require('gulp-concat-util');
_.rename = require('gulp-rename');
_.config = {};
_.pug = require('gulp-pug');
_.stylus = require('gulp-stylus');
_.koutoSwiss = require('kouto-swiss');
_.jeet = require('jeet');
_.insert = require('gulp-insert');
_.urlAdjuster = require('gulp-css-url-adjuster');
_.gulpif = require('gulp-if');
_.colors = require('colors');
_.buffer = require('vinyl-buffer');
_.merge = require('merge-stream');
_.imageResize = require('gulp-image-resize');
_.pixelsmith = require('pixelsmith');
_.spritesmith = require('gulp.spritesmith');
_.sprity = require('sprity');
_.$ = require('gulp-load-plugins')();
_.svgstore = require('gulp-svgstore');
_.svgmin = require('gulp-svgmin');
//_.cache = require('gulp-cache');
_.cached = require('gulp-cached');
//_.remember = require('gulp-remember');
_.progeny = require('gulp-progeny');

//_.pugInheritance = require('gulp-pug-inheritance');

_.trycatch(function() {
    _.config = require('./local/config');
}, function(err) {
    console.error('Can\'t locate "local" dir! Exit now.'.red);
    process.exit(1);
});

console.info('BX-Front-Builder '.zebra + "\n" + 'ver. ' + _.appVer + "\n" + 'Start...');

if(_.config.path.source) {
    _.gulp.localSourcePath = './local/' + _.config.path.source;
} else {
    _.gulp.localSourcePath = './local/src';
    /*
    console.error('Source directory path is not specified in "config.json"! Exit now.');
    process.exit(1);
    */
}

//_.emitty = require('emitty').setup(_.gulp.localSourcePath, 'pug');

if(_.config.path.distributive) {
    _.gulp.localDistributivePath = './local/' + _.config.path.distributive;
} else {
    _.gulp.localDistributivePath = './local/dist';
    /*
    console.error('Distributive directory path is not specified in "config.json"! Exit now.');
    process.exit(1);
    */
}

if(_.config.path.markup) {
    _.gulp.markupPath = _.config.path.markup;
} else {
    console.error('Markup directory path is not specified in "config.json"! Exit now.');
    process.exit(1);
}

if(_.config.path.markupInclude) {
    _.gulp.markupInclude = _.config.path.markupInclude;
} else {
    console.error('Markup includes directories path is not specified in "config.json"! Exit now.');
    process.exit(1);
}

if(_.config.path.assets) {
    _.gulp.localAssetsPath = './local/' + _.config.path.assets;
} else {
    _.gulp.localAssetsPath = './local/assets';
    /*
     console.error('Distributive directory path is not specified in "config.json"! Exit now.');
     process.exit(1);
     */
}

_.requireDir('core/tasks', { recurse: true });

_.gulp.task('default', function () {

    //console.log(config);
    //console.info('Ok.'.blue);

    console.info('Have no default actions.'.magenta + "\n");
    console.info('List of avail. actions:'.magenta + "\n");

    console.info('1. build'.green + "\n");
    console.info('2. markup'.green + "\n");
    console.info('3. styles'.green + "\n");
    console.info('4. scripts'.green + "\n");
    console.info('5. styles:main'.green + "\n");
    console.info('6. scripts:main'.green + "\n");
    console.info('7. scripts:vendor'.green + "\n");
    console.info('8. files'.green + "\n");
    console.info('9. core'.green + "\n");


});

_.gulp.task('build', function (callback) {

    _.runSequence(
        ['core:scripts:collect:core','local:scripts:collect:vendor','local:sprite:dist', 'local:sprite:svg:dist'],
        ['local:styles:collect:main','local:styles:collect:vendor','core:scripts:copy:dist', 'local:markup:dist','local:scripts:dist','local:vendor:dist'],
        ['dist:markup:dest', 'dist:scripts:dest', 'dist:styles:dest','local:assets:dest','dist:vendor:dest','local:sprite:dest', 'local:sprite:svg:dest'],
        callback
    );

});

_.gulp.task('core', function (callback) {

    _.runSequence(
        ['core:scripts:collect:core'],
        ['core:scripts:copy:dist'],
        ['dist:scripts:dest'],
        callback
    );

});

_.gulp.task('scripts', function (callback) {

    _.runSequence(
        ['core:scripts:copy:dist','local:scripts:collect:vendor'],
        ['local:scripts:dist'],
        ['dist:scripts:dest'],
        callback
    );

});

_.gulp.task('scripts:vendor', function (callback) {

    _.runSequence(
        ['local:scripts:collect:vendor'],
        ['dist:scripts:dest'],
        callback
    );

});

_.gulp.task('scripts:main', function (callback) {

    _.runSequence(
        ['local:scripts:dist'],
        ['dist:scripts:dest'],
        callback
    );

});

_.gulp.task('styles', function (callback) {

    _.runSequence(
        ['local:sprite:dist'],
        ['local:styles:collect:main','local:styles:collect:vendor'],
        ['local:vendor:dist'],
        ['dist:styles:dest','dist:vendor:dest'],
        callback
    );

});

_.gulp.task('styles:main', function (callback) {

    _.runSequence(
        ['local:sprite:dist'],
        ['local:styles:collect:main'],
        ['dist:styles:dest'],
        callback
    );

});

_.gulp.task('markup', function (callback) {

    _.runSequence(
        ['local:markup:dist'],
        ['dist:markup:dest'],
        callback
    );

});

_.gulp.task('files', function (callback) {

    _.runSequence(
        ['local:vendor:dist'],
        ['local:assets:dest','dist:vendor:dest'],
        callback
    );

});


_.gulp.task('watch', function() {

    global.watch = true;

    _.gulp.watch('local/assets/**/*', ['local:assets:dest']);

    _.gulp.watch(_.gulp.localSourcePath + '/**/*.js', function() {
        _.runSequence(
            ['local:scripts:dist'],
            ['dist:scripts:dest']
        );
    });

    _.gulp.watch('local/sprites/**/*', function() {
        _.runSequence(
            ['local:sprite:dist'],
            ['local:sprite:dest']
        );
    });

    _.gulp.watch('local/sprites-svg/**/*', function() {
        _.runSequence(
            ['local:sprite:svg:dist'],
            ['local:sprite:svg:dest']
        );
    });

    _.gulp.watch(_.gulp.localSourcePath + '/**/*.pug', function() {
        _.runSequence(
            ['local:markup:dist'],
            ['dist:markup:dest']
        );
    });


    // _.gulp.watch(_.gulp.localSourcePath + '/**/*.pug', function(event) {
    //     //console.log(global.emittyChangedFile);
    //     //global.emittyChangedFile = event.path;
    //     _.runSequence(
    //         ['w2t'],
    //         ['dist:markup:dest']
    //     );
    // });


    _.gulp.watch(_.gulp.localSourcePath + '/**/*.styl', function() {
        _.runSequence(
            ['local:styles:collect:main'],
            ['dist:styles:dest']
        );
    });
});

// _.gulp.task('w2t', () =>
//     _.gulp.src(_.gulp.localSourcePath + '/**/*.pug')
//         .pipe(_.gulpif(global.watch, _.emitty.stream()))
//         .pipe(_.pug({
//             pretty: true
//         }))
//         .pipe(_.gulp.dest(_.gulp.localDistributivePath + '/markups'))
// );